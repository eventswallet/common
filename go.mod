module gitlab.com/eventswallet/common

go 1.16

require (
	github.com/joho/godotenv v1.4.0
	github.com/stretchr/testify v1.7.0
	gopkg.in/ini.v1 v1.66.2
)
